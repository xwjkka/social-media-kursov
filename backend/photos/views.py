# from rest_framework.response import Response
# from rest_framework.decorators import api_view
# from django.core import serializers
from rest_framework.viewsets import ModelViewSet
from photos.models import Photo
from photos.serializers import PhotoSerializer


class PhotoViewSet(ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
# @api_view()
# def photos__list(request):
#     photos = Photo.objects.all()
#     data = PhotoSerializer(instance=photos, many=True).data
#     return Response(data={'data': data})