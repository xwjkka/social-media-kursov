from django.db import models
from authentication.models import User

# Create your models here.

class Photo(models.Model):
    user_id = models.ForeignKey(User, on_delete = models.CASCADE, null=True)
    path = models.ImageField(verbose_name='Фото', upload_to='photos')
    description = models.TextField(verbose_name='Описание')

    def __str__(self):
        return self.description
    
    class Meta:
        verbose_name = 'Фотография'
        verbose_name_plural = 'Фотографии'